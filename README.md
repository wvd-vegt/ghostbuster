# Project Description #

This application enumerates all devices, detects ghosted devices and removes these if they match selectable devices types and/or devices classes with a single mouse-click. 

  ![gb3.png](https://bitbucket.org/repo/qE7Kbro/images/362840773-gb3.png)

In above screenshot the yellow colored devices are set to be uninstalled when they become ghosted. Both device and class name selection can be used. 

If you like this tool, leave me a note, rate this project or write a review or [Donate to Ghostbuster](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ZZLQ2FAAN757U).

  [![paypal.png](https://bitbucket.org/repo/qE7Kbro/images/1378486-paypal.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ZZLQ2FAAN757U)

If you've encountered problems, leave a (detailed) issue in the [Issue Tracker](https://bitbucket.org/wvd-vegt/ghostbuster/issues?status=new&status=open).

# Downloads #

GhostBuster [installers](https://wvd-vegt.bitbucket.io/).

# Forking #

I'm NOT a big fan of forking and specifically creating repository copies at GitHub (as the sources end-up all over the place and in various states of decay). So please do not fork. 
If you want to contribute, please post or mail changes for review.

# Latest release #
 
* CS 111833
* I have some issues syncing sources with TFS (last changeset lacks).
* Re-uploaded downloads after fixing a small but sometimes show stopping command-line issue (the command-line help message box shows when going elevated). Please update if you encounter this issue.
* Removed default event log creation to make GhostBuster more portable and solve a policy/access issue.

* An existing GhostBuster event log can be removed with a PowerShell command:

       remove-eventlog -LogName "GhostBuster"

* A new command-line option /EVENTLOG has been added to manually create a GhostBuster event log. 
* Set version to 1.0.7 
* See the Documentation and Advanced Options for more information on these new features. 
* The screenshot below is also updated, for more screenshots see Screenshots.

# Previous releases #

* CS 110700.
* Updated code that shows a devices properties (context menu). 
* Added new code to release comport that are removed but still in use.
* Note: it will not repair missing reservation in the comportdb (yet). 
* Added opening the registry for either the device or the services key (if present).
* Note: due to technical limitations regedit must be closed before viewing a new devices registry entries. 
* Made it possible to lift the services barrier and remove them too (Ini File). It will probably not remove the device's service that was detected and led to the labeling as service. 
* New Installer based on Wix/WixBuilder.
* Note: depending on the installation type, the location of the Ghostbuster.ini file will vary.
* Either '%appdata%\GhostBuster' or '%allusersprofile%\Ghostbuster' will be used. 
* Note: In the new serial port dialog a COM99 serial port shows up. 
* This is some left-over from testing I will remove in a next release. 
* Set version to 1.0.5. 

* CS 106145 
* Changed click-once update to application exit. 
* Fixed certificate. 
* Updated PayPal done button. 
* Added menu item for click-once updates check. 
* Added menu item to visit website. 
* Set automatic update check to application exit. 
* Set version to 1.0.4.0. 

* CS 98926 
* Added a Portable Executable for Windows XP merged against .Net 4 Full Framework. 
* Improved System Restore Detection (search for rstrui.exe). 
* Flagged null Guid device as system so it cannot be removed anymore. 
* Added <No device class specified> to devices with an empty device class. 
* Set copyright to 2012. 
* Version set to v1.0.2.0. 
* Added Click-Once Release. 

* CS 91520 
* Added WMI based Restore Point support. 
* Note this is experimental code, it creates a 'GhostBuster Restore Point'. 
* Removed test code from program.cs. 
* Improved counting. 
* Changed color of ghosted but unfiltered devices. 
* Changed HwEntries into an ObservableCollection. 
* Added Properties Form. 
* Added Properties Menu Item to Right-Click Context Menu. 
* It shows a dialog with some more information on the driver properties found in the registry. 
* Added Hide Unfiltered Devices to Right-Click Context Menu. 
* This will hide all unfiltered and non ghosted devices and will condense the list's size. 

* CS 80445
* Command line support. 
* Event log output. 
* Task scheduler support (thanks to http://taskscheduler.codeplex.com). 
* The scheduled task can be used to run GhostBuster without further UAC prompts. 

* CS 77227
* Fixed broken wildcard device removal. 

* CS 76647
* Added status bar. 
* Added wildcard support (* and ? are supported as wildcards). 
* Added Friendly Name / Device Description Column. 
* Added column showing the match type. 
* Improved performance 
* Improved coloring of matching items 

* CS 74907
* Removed non functional checkboxes (right click a device for the context menu). 
* Now compatible with UAC (Vista/Windows 7). 
* Now compatible with WHS and W2K3 Server. 
* Some user-interface enhancements. 
* Portable version. 
* Added some help / tooltips. 
* Correctly Enable and Disable Context Menu Items. 
* Ini File now created in %AppData%\GhostBuster. 
* About Ghosted Devices:
* By default Windows does not show ghosted devices. Windows will show Ghosted devices only if two criteria are met:
* An environment variable DEVMGR_SHOW_NONPRESENT_DEVICES with a value of 1 is added 
* In Device Manager (Start Menu & Run 'compmgmt.msc' ) if you check the View|Hidden Devices menu item. 
* Because Windows has to detect if a devices is ghosted by trying to start it, a large number of ghosted devices will add to the startup time of Windows. The 'regular' way of removing these devices is right clicking each of them, select the uninstall menu item and wait for the Device Manager to uninstall the device and rebuild it's tree of devices. 

* Ghostbuster does this all in one click and removes all the ghosted devices matching your own selection criteria with a single click! 

* You can select and deselect classes or devices through the right-click context menu of the devices list view. Only ghosted devices (grayed text) AND devices that match the selection criteria (light yellow background) will be uninstalled when the 'Remove Ghosts' button is pressed. All other devices remain untouched. 

* Beware that windows has a fair number of devices that are ghosted but should not be uninstalled (like devices in the sound, video and game controller class or system and non plug-n-play devices). Ghostbuster marks most of these as services.
* Warning: So use it with care and only if you know what you are doing! 

# Some reviews (after being added to Softpedia, people started to find GhostBuster) #
* [remove-uninstall-hiddenghosted-devices](http://www.addictivetips.com/windows-tips/remove-uninstall-hiddenghosted-devices/)
* [enumerate-detect-remove-ghosted-devices-with-ghostbuster](http://www.blogsdna.com/6248/enumerate-detect-remove-ghosted-devices-with-ghostbuster.htm)
* [rimuovere-e-disinstallare-driver](http://www.navigaweb.net/2009/05/rimuovere-e-disinstallare-driver.html)
* [como-remover-o-desinstalar-dispositivos-fantasma](http://www.adslfaqs.com.ar/como-remover-o-desinstalar-dispositivos-fantasmas/)
